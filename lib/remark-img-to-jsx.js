import { visit } from 'unist-util-visit'
import sizeOf from 'image-size'
import fs from 'fs'

var log = require('next/dist/build/output/log')

function getBaseHost() {
  let host = ''

  if (!Object.prototype.hasOwnProperty.call(process.env, 'CI_PAGES_URL')) {
    let hostName = process.argv.indexOf('-H')
    let portNumber = process.argv.indexOf('-p')

    hostName = (hostName >= 0 ? process.argv[hostName + 1] : 'localhost').replace(
      new RegExp('^http(s?)://'),
      ''
    )
    portNumber = (portNumber >= 0 ? process.argv[portNumber + 1] : '3000').replace(
      new RegExp('^http(s?)://'),
      ''
    )
    host = `http://${hostName}:${portNumber}`

    log.warn('Please set the "CI_PAGES_URL" environment variable with your domain and basepath.')
  } else {
    host = process.env.CI_PAGES_URL
  }

  return host
}

/**
 * Builds an uri path from an array
 * @param parts
 * @returns {*}
 */
function joinUri(parts) {
  return parts.map((value) => value.replace(/\/$|^\//, '')).join('/')
}

/**
 * Checks if the path looks like a static file,
 *  example: "/static/" or "static/"
 * @param path
 * @returns {boolean}
 */
function isStaticFile(path) {
  return typeof path === 'string' && /^(\/)?static/.test(path)
}

/**
 * Returns the absolute path if the path corresponds to a static file
 * @param path
 * @returns {*}
 */
function getImagePath(path) {
  if (isStaticFile(path)) {
    path = joinUri([getBaseHost(), path])
  }

  return path
}

export default function remarkImgToJsx() {
  return (tree) => {
    visit(
      tree,
      // only visit p tags that contain an img element
      (node) => node.type === 'paragraph' && node.children.some((n) => n.type === 'image'),
      (node) => {
        const imageNode = node.children.find((n) => n.type === 'image')
        const imageSrc = joinUri(['public', imageNode.url])

        // only local files
        if (fs.existsSync(`${process.cwd()}/${imageSrc}`)) {
          const dimensions = sizeOf(`${process.cwd()}/${imageSrc}`)

          // Convert original node to next/image
          ;(imageNode.type = 'mdxJsxFlowElement'),
            (imageNode.name = 'Image'),
            (imageNode.attributes = [
              { type: 'mdxJsxAttribute', name: 'alt', value: imageNode.alt },
              { type: 'mdxJsxAttribute', name: 'src', value: getImagePath(imageNode.url) },
              { type: 'mdxJsxAttribute', name: 'width', value: dimensions.width },
              { type: 'mdxJsxAttribute', name: 'height', value: dimensions.height },
            ])

          // Change node type from p to div to avoid nesting error
          node.type = 'div'
          node.children = [imageNode]
        }
      }
    )
  }
}
